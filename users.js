const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: ["Masters"],
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


// Q1 Find all users who are interested in playing video games.

let usersInterstedinVideoGames = Object.keys(users).filter(items => {

    if (users[items]["interests"].toString().includes("Video Games")) {
        return true;
    }
    else {
        return false;
    }
})
console.log(`Users interested in playing Video Games are : ${usersInterstedinVideoGames}`);




// Q2 Find all users staying in Germany.

let usersStayinginGermany = Object.keys(users).reduce((acc,items) => {
    if (users[items]["nationality"].includes("Germany")) {
        acc.push(items)
    }
   return acc;
},[])

console.log(`Users staying in Germany are : ${usersStayinginGermany}`);



// Q3 Sort users based on their seniority level 
// for Designation - Senior Developer > Developer > Intern
// for Age - 20 > 10

let sortedUsersWithSeniorityLevel =Object.keys(users).filter(items=>{
        if(users[items]["age"] >= 10 && users[items]["age"] <= 20){
            return true;
        }

}).Object.keys(users).sort((a,b)=>{
        if(users[a]["desgination"] > users[b]["desgination"] && users[a]){
            return -1;
        }
})

console.log(sortedUsersWithSeniorityLevel);



// Q4 Find all users with masters Degree.

let usersWithMasterDegree = Object.keys(users).filter(items => {
    if (users[items]["qualification"].includes("Masters")) {
        return true;
    }
    else {
        return false;
    }
})

console.log(`Users who have masters degree are : ${usersWithMasterDegree}`);


// Q5 Group users based on their Programming language mentioned in their designation.

